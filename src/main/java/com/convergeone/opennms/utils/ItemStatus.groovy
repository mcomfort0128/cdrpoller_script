package com.convergeone.opennms.utils

class ItemStatus implements Comparable<ItemStatus> {
  private static final long serialVersionUID = 46869417416527L;
  private String itemId
  private String itemName
  private String itemType;
  private String status;

  ItemStatus() {}

  String getItemId() {
    return itemId
  }

  void setItemId(String itemId) {
    this.itemId = itemId
  }

  String getItemName() {
    return itemName
  }

  void setItemName(String itemName) {
    this.itemName = itemName
  }

  @Override
  int compareTo(ItemStatus itemStatus) {
    ItemStatus anotherItem = (ItemStatus) itemStatus;
    if (itemStatus != null && anotherItem != null) {
      if (this.getItemId() != 0) {
        return this.getItemId().compareTo(anotherItem.getItemId());
      } else if (!this.getItemName().equals("")) {
        return this.getItemName().compareTo(anotherItem.getItemName())
      }
    }
  }

  String getItemType() {
    return itemType
  }

  void setItemType(String statusName) {
    this.itemType = statusName
  }

  String getStatus() {
    return status
  }

  void setStatus(String status) {
    this.status = status
  }

  boolean equals(o) {
    if (this.is(o)) return true
    if (!(o instanceof ItemStatus)) return false

    ItemStatus that = (ItemStatus) o

    if (itemId != that.itemId) return false
    if (itemName != that.itemName) return false
    if (itemType != that.itemType) return false
    if (status != that.status) return false

    return true
  }

  int hashCode() {
    int result
    result = (itemId != null ? itemId.hashCode() : 0)
    result = 31 * result + (itemName != null ? itemName.hashCode() : 0)
    result = 31 * result + (itemType != null ? itemType.hashCode() : 0)
    result = 31 * result + (status != null ? status.hashCode() : 0)
    return result
  }

  @Override
  public String toString() {
    return "ItemStatus{" +
      "itemId='" + itemId + '\'' +
      ", itemName='" + itemName + '\'' +
      ", itemType='" + itemType + '\'' +
      ", status='" + status + '\'' +
      '}';
  }
}
