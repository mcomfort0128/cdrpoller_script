package com.convergeone.opennms.utils;

public class Constants {
    public static String DEVICE_HEALTH_QUERY = "device.health.query";
    public static String DEVICE_QUERY = "device.query";
    public static String SIP_QUERY = "sip.query";
    public static final String TELEPRESENCE_MCU = "Telepresence MCU";
    public static final String TELEPRESENCE_ENDPOINT = "Telepresence Endpoint";
    public static final String TELEPRESENCE_CDR = "Telepresence CDR";

    //device.health.query
    public static String CPU_LOAD = "cpuLoad";
    public static String MEDIA_LOAD = "mediaLoad";
    public static String AUDIO_LOAD = "audioLoad";
    public static String VIDEO_LOAD = "videoLoad";
    public static String FAN_STATUS = "fanStatus";
    public static String FAN_STATUS_WORST = "fanStatusWorst";
    public static String TEMPERATURE_STATUS = "temperatureStatus";
    public static String TEMPERATURE_STATUS_WORST = "temperatureStatusWorst";
    public static String BATTERY_STATUS = "rtcBatteryStatus";
    public static String BATTERY_STATUS_WOSRT = "rtcBatteryStatusWorst";
    public static String VOLTAGE_STATUS = "voltagesStatus";
    public static String VOLTAGE_STATUS_WORST = "voltagesStatusWorst";
    public static String OPERATIONAL_STATUS = "operationalStatus";

    //device.health.query item id
    public static String CPU_LOAD_CODE = "CPU";
    public static String MEDIA_LOAD_CODE = "MEDIA";
    public static String AUDIO_LOAD_CODE = "AUDIO";
    public static String VIDEO_LOAD_CODE = "VIDEO";
    public static String FAN_STATUS_CODE = "FAN";
    public static String FAN_STATUS_WORST_CODE = "FAN_WST";
    public static String TEMPERATURE_STATUS_CODE = "TEMP";
    public static String TEMPERATURE_STATUS_WORST_CODE = "TEMP_WST";
    public static String BATTERY_STATUS_CODE = "BATT";
    public static String BATTERY_STATUS_WOSRT_CODE = "BATT_WST";
    public static String VOLTAGE_STATUS_CODE = "VOLT";
    public static String VOLTAGE_STATUS_WORST_CODE = "VOLT_WST";
    public static String OPERATIONAL_STATUS_CODE = "OPERA";

    //device.health.query item name
    public static String CPU_LOAD_NAME = "cpu load";
    public static String MEDIA_LOAD_NAME = "media load";
    public static String AUDIO_LOAD_NAME = "audio load";
    public static String VIDEO_LOAD_NAME = "video load";
    public static String FAN_STATUS_NAME = "fan";
    public static String FAN_STATUS_WORST_NAME = "fan worst";
    public static String TEMPERATURE_STATUS_NAME = "temperature";
    public static String TEMPERATURE_STATUS_WORST_NAME = "temperature worst";
    public static String BATTERY_STATUS_NAME = "battery";
    public static String BATTERY_STATUS_WOSRT_NAME = "battery worst";
    public static String VOLTAGE_STATUS_NAME = "voltages";
    public static String VOLTAGE_STATUS_WORST_NAME = "voltages worst";
    public static String OPERATIONAL_STATUS_NAME= "operation";

    //device.health.query item type
    public static String CPU_LOAD_TYPE = "cpuPercent";
    public static String MEDIA_LOAD_TYPE = "medCapPercent";
    public static String AUDIO_LOAD_TYPE= "audCapPercent";
    public static String VIDEO_LOAD_TYPE = "vidCapPercent";
    public static String FAN_STATUS_TYPE = "fanSeverity";
    public static String FAN_STATUS_WORST_TYPE = "fanWstSeverity";
    public static String TEMPERATURE_STATUS_TYPE = "temperature";
    public static String TEMPERATURE_STATUS_WORST_TYPE = "tempWstSeverity";
    public static String BATTERY_STATUS_TYPE = "battSeverity";
    public static String BATTERY_STATUS_WOSRT_TYPE = "battWstSeverity";
    public static String VOLTAGE_STATUS_TYPE = "voltSeverity";
    public static String VOLTAGE_STATUS_WORST_TYPE = "voltWstSeverity";
    public static String OPERATIONAL_STATUS_TYPE= "operationStatus";


    //device.query
    public static String CURRENT_TIME = "currentTime";
    public static String RESTART_TIME = "restartTime";
    public static String SERIAL = "serial";
    public static String SOFTWARE_VERSION = "softwareVersion";
    public static String BUILD_VERSION = "buildVersion";
    public static String MODEL = "model";
    public static String API_VERSION = "apiVersion";
    public static String ACTIVATE_FEATURES = "activatedFeatures";
    public static String ACTIVATE_LICENCES = "activatedLicenses";
    public static String LICENCES = "license";
    public static String PORTS = "ports";
    public static String EXPIRY = "expiry";
    public static String CLUSTER_TYPE = "clusterType";
    public static String MAX_CONFERENCE_SIZE = "maxConferenceSize";
    public static String TOTAL_VIDEO_PORTS = "totalVideoPorts";
    public static String TOTAL_AUDIO_ONLY_PORTS = "totalAudioOnlyPorts";
    public static String TOTAL_STREAMING_CONTENT_PORTS = "totalStreamingAndContentPorts";
    public static String PORT_RESERVATION_MODE = "portReservationMode";
    public static String MAX_VIDEO_SOLUTION = "maxVideoResolution";
    public static String VIDEO_PORT_ALLOCATION = "videoPortAllocation";
    public static String SHUTDOWN_STATUS = "shutdownStatus";
    public static String REBOOT_REQUIRED = "rebootRequired";
    public static String FINISHED_BOOTING = "finishedBooting";
    public static String MEDIA_RESOURCE = "mediaResources";
    public static String MEDIA_RESOURCE_RESTART = "mediaResourceRestarts";

   //device.query code
   public static String CURRENT_TIME_CODE = "CUR_TIME";
    public static String RESTART_TIME_CODE = "RE_TIME";
    public static String SERIAL_CODE = "SER";
    public static String SOFTWARE_VERSION_CODE = "VER";
    public static String BUILD_VERSION_CODE = "BUILD_VER";
    public static String MODEL_CODE = "MODEL";
    public static String API_VERSION_CODE = "API_VER";
    public static String ACTIVATE_FEATURES_CODE = "ACT_FEA";
    public static String ACTIVATE_LICENCES_CODE = "ACT_LIC";
    public static String LICENCES_CODE = "LIC";
    public static String PORTS_CODE = "PORT";
    public static String EXPIRY_CODE = "EXP";
    public static String CLUSTER_TYPE_CODE = "CLUS_TYP";
    public static String MAX_CONFERENCE_SIZE_CODE = "MAX_CONF";
    public static String TOTAL_VIDEO_PORTS_CODE = "TO_VI_PRT";
    public static String TOTAL_AUDIO_ONLY_PORTS_CODE = "TO_AU_PRT";
    public static String TOTAL_STREAMING_CONTENT_PORTS_CODE = "TO_ST_PRT";
    public static String PORT_RESERVATION_MODE_CODE = "PRT_RE";
    public static String MAX_VIDEO_SOLUTION_CODE = "MAX_VI_S";
    public static String VIDEO_PORT_ALLOCATION_CODE = "VI_PRT_AL";
    public static String SHUTDOWN_STATUS_CODE = "SHUS";
    public static String REBOOT_REQUIRED_CODE = "RERE";
    public static String FINISHED_BOOTING_CODE = "FIBO";
    public static String MEDIA_RESOURCE_CODE = "MEDSO";
    public static String MEDIA_RESOURCE_RESTART_CODE = "MEDSORE";

    //device.query code
    public static String CURRENT_TIME_NAME = "current time";
    public static String RESTART_TIME_NAME = "restart time";
    public static String SERIAL_NAME = "serial";
    public static String SOFTWARE_VERSION_NAME = "software version";
    public static String BUILD_VERSION_NAME = "build version";
    public static String MODEL_NAME = "model name";
    public static String API_VERSION_NAME = "api version";
    public static String ACTIVATE_FEATURES_NAME = "activate feature";
    public static String ACTIVATE_LICENCES_NAME = "activate license";
    public static String LICENCES_NAME= "license";
    public static String PORTS_NAME = "port name";
    public static String EXPIRY_NAME = "experation date";
    public static String CLUSTER_TYPE_NAME= "cluster type";
    public static String MAX_CONFERENCE_SIZE_NAME = "max conf size";
    public static String TOTAL_VIDEO_PORTS_NAME = "total video port";
    public static String TOTAL_AUDIO_ONLY_PORTS_NAME = "total audio port";
    public static String TOTAL_STREAMING_CONTENT_PORTS_NAME = "total stream port";
    public static String PORT_RESERVATION_MODE_NAME = "port reservation";
    public static String MAX_VIDEO_SOLUTION_NAME = "max video solution";
    public static String VIDEO_PORT_ALLOCATION_NAME = "video port allocation";
    public static String SHUTDOWN_STATUS_NAME = "shutdown status";
    public static String REBOOT_REQUIRED_NAME = "reboot status";
    public static String FINISHED_BOOTING_NAME = "finish booting";
    public static String MEDIA_RESOURCE_NAME = "media resource";
    public static String MEDIA_RESOURCE_RESTART_NAME = "media restart";

    //device.query type
    public static String CURRENT_TIME_TYPE = "currTime";
    public static String RESTART_TIME_TYPE = "restTime";
    public static String SERIAL_TYPE = "serNum";
    public static String SOFTWARE_VERSION_TYPE = "softwareVer";
    public static String BUILD_VERSION_TYPE = "buildVer";
    public static String MODEL_TYPE = "modelName";
    public static String API_VERSION_TYPE = "apiVersion";
    public static String ACTIVATE_FEATURES_TYPE = "activateFea";
    public static String ACTIVATE_LICENCES_TYPE = "activateLic";
    public static String LICENCES_TYPE= "licNum";
    public static String PORTS_TYPE = "numLicPrt";
    public static String EXPIRY_TYPE= "expDate";
    public static String CLUSTER_TYPE_TYPE= "clusType";
    public static String MAX_CONFERENCE_SIZE_TYPE = "maxCofSize";
    public static String TOTAL_VIDEO_PORTS_TYPE = "totViPrt";
    public static String TOTAL_AUDIO_ONLY_PORTS_TYPE = "totAudprt";
    public static String TOTAL_STREAMING_CONTENT_PORTS_TYPE = "totalStrPrt";
    public static String PORT_RESERVATION_MODE_TYPE = "portRes";
    public static String MAX_VIDEO_SOLUTION_TYPE= "maxVidSolu";
    public static String VIDEO_PORT_ALLOCATION_TYPE = "vidPrtAllo";
    public static String SHUTDOWN_STATUS_TYPE = "shutdownStat";
    public static String REBOOT_REQUIRED_TYPE= "rebootStat";
    public static String FINISHED_BOOTING_TYPE = "finiBooting";
    public static String MEDIA_RESOURCE_TYPE= "mediaResou";
    public static String MEDIA_RESOURCE_RESTART_TYPE = "mediarestart";

    //sip.query
    public static String OUTBOUND_CONFIGURATION = "outboundConfiguration";
    public static String OUTBOUND_ADDRESS = "outboundAddress";
    public static String OUTBOUND_DOMAIN = "outboundDomain";
    public static String CONFIGURATION_REGISTRAR = "configuredRegistrar";
    public static String CONFIGURATION_PROXY = "configuredProxy";
    public static String CONFIGURATION_CONTACT_URL = "registrarContactURI";
    public static String CONFIGURATION_CONTACT_DOMAIN = "registrarContactDomain";
    public static String CONFERENCE_REGISTRATION = "conferenceRegistration";
    public static String REGISTRAR_UASAGE = "registrarUsage";
    public static String REGISTRAR_TYPE = "registrarType";
    public static String MAX_OCS_BITRATE = "maxOcsBitrate";
    public static String OUTGOING_TRANSPORT = "outgoingTransport";
    public static String USE_LOCAL_CERTIFICATE = "useLocalCertificate";
    public static String REGISTRATION_STATUS = "registrationStatus";

    //sip.query code
    public static String OUTBOUND_CONFIGURATION_CODE = "O_CONFIG";
    public static String OUTBOUND_ADDRESS_CODE = "O_ADDRESS";
    public static String OUTBOUND_DOMAIN_CODE = "O_DOMAIN";
    public static String CONFIGURATION_REGISTRAR_CODE = "CONFIG_REG";
    public static String CONFIGURATION_PROXY_CODE = "CONFIG_PROX";
    public static String CONFIGURATION_CONTACT_URL_CODE = "REG_URL";
    public static String CONFIGURATION_CONTACT_DOMAIN_CODE = "REG_DOMAIN";
    public static String CONFERENCE_REGISTRATION_CODE = "CONF_REG";
    public static String REGISTRAR_USAGE_CODE = "REG_USAGE";
    public static String REGISTRAR_TYPE_CODE = "REG_TYPE";
    public static String MAX_OCS_BITRATE_CODE = "MAX_RATE";
    public static String OUTGOING_TRANSPORT_CODE = "O_TRANSPORT";
    public static String USE_LOCAL_CERTIFICATE_CODE = "USE_CERT";
    public static String REGISTRATION_STATUS_CODE = "REG_STAT";

    //sip.query name
    public static String OUTBOUND_CONFIGURATION_NAME = "outbound config";
    public static String OUTBOUND_ADDRESS_NAME = "outbound address";
    public static String OUTBOUND_DOMAIN_NAME = "outbound domain";
    public static String CONFIGURATION_REGISTRAR_NAME = "config registration";
    public static String CONFIGURATION_PROXY_NAME = "config proxy";
    public static String CONFIGURATION_CONTACT_URL_NAME = "registration url";
    public static String CONFIGURATION_CONTACT_DOMAIN_NAME = "registration domain";
    public static String CONFERENCE_REGISTRATION_NAME = "conference registration";
    public static String REGISTRAR_USAGE_NAME = "registration usage";
    public static String REGISTRAR_TYPE_NAME = "registration type";
    public static String MAX_OCS_BITRATE_NAME = "max bit rate";
    public static String OUTGOING_TRANSPORT_NAME = "outgoing transport";
    public static String USE_LOCAL_CERTIFICATE_NAME = "use local cert";
    public static String REGISTRATION_STATUS_NAME = "registration status";

    //sip.query type
    public static String OUTBOUND_CONFIGURATION_TYPE = "outConfig";
    public static String OUTBOUND_ADDRESS_TYPE = "outAddress";
    public static String OUTBOUND_DOMAIN_TYPE = "outDomain";
    public static String CONFIGURATION_REGISTRAR_TYPE = "configReg";
    public static String CONFIGURATION_PROXY_TYPE= "confProx";
    public static String CONFIGURATION_CONTACT_URL_TYPE = "regRRL";
    public static String CONFIGURATION_CONTACT_DOMAIN_TYPE = "regDomain";
    public static String CONFERENCE_REGISTRATION_TYPE = "confReg";
    public static String REGISTRAR_USAGE_TYPE = "regUsage";
    public static String REGISTRAR_TYPE_TYPE= "regType";
    public static String MAX_OCS_BITRATE_TYPE = "maxBitRate";
    public static String OUTGOING_TRANSPORT_TYPE = "outTransport";
    public static String USE_LOCAL_CERTIFICATE_TYPE = "useLocalCert";
    public static String REGISTRATION_STATUS_TYPE= "regStatus";
}
