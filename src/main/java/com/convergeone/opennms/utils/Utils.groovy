package com.convergeone.opennms.utils

import groovy.sql.Sql

@GrabConfig(systemClassLoader = true)
@Grab(group = 'org.postgresql', module = 'postgresql', version = '9.3-1100-jdbc4')

public class Utils {
  private final String prop_path = "/opt/onguard/etc/auditProperties.xml"
  private final insert_endpoint_status = "INSERT INTO device_status (ipaddr, item_id, item_name, item_type, status) values (?, ?, ?, ?,?)"
  private final delete_endpoint_status = "DELETE FROM device_status where ipaddr=?"
  private final insert_endpoint_meta = "WITH upd AS (UPDATE device_meta SET last_collected = now() WHERE ipaddr = ?.ip RETURNING * ) INSERT INTO device_meta (ipaddr, etype) (SELECT ?.ip, ?.etype WHERE NOT EXISTS (select * from upd)); "
  private EndpointItems endpointItems = null
  private boolean isRecordExists = false;

  def queryIPAddressForTelepresence(String categoryname) {
    List<String> ipAddress = new ArrayList<>()
    def sql = Sql.newInstance("jdbc:postgresql://localhost:5432/opennms", "opennms", "opennms", "org.postgresql.Driver")
    List ips = sql.rows("SELECT DISTINCT ips.ipaddr FROM node n NATURAL JOIN ipinterface ips NATURAL JOIN category_node cn NATURAL JOIN categories c WHERE  n.nodeid=ips.nodeid and cn.categoryid = c.categoryid and c.categoryname="+ "'" + categoryname + "'")
    ips.each {
      def ip = it.ipaddr
      ipAddress.add(ip)
    }
    return ipAddress
  }

  def getNodeInfo(String catagory) {
    def ip, userId, password, serverType;
    List<String> ipAddress = queryIPAddressForTelepresence(catagory);
    List<NodeInfo> nodeInfo = new ArrayList<>();
    def auditConfig = new XmlSlurper().parse(prop_path)
    if (auditConfig == null || !auditConfig) {
      println("Can't parse the auditProperties.xml in path " + prop_path)
    }
    for (String s : ipAddress) {
      ip = auditConfig.node.find { it.host.text() == s }
      if (ip) {
        userId = ip.username.text();
        password = ip.password.text();
        serverType = ip.serverType.text(); ;
      }
      NodeInfo infoNode = new NodeInfo(s, userId, password, serverType);
      nodeInfo.add(infoNode)
    }
    return nodeInfo;
  }

  def connectPostgreDatabse() {
    def sql = null
    def retries = 10
    while (sql == null && retries-- > 0) {
      try {
        sql = Sql.newInstance("jdbc:postgresql://localhost:5432/device_status", "postgres", "", "org.postgresql.Driver")
      } catch (Exception e) {
        print("Can't connect to the database")
      }
      try {
        Thread.sleep(50 * Math.random())
      } catch (Exception ex) {
      }
    }
    sql
  }

  def insertOrUpdateEndpointStatus(def sql, EndpointItems endpointItems) {
    try {
      sql.execute(delete_endpoint_status, endpointItems.getIpAddress());
    } catch (Exception ex) {
      println("Failed to delete record: " + ex.getMessage())
    }
    for (ItemStatus item : endpointItems.getAlItems()) {
      try {
        sql.execute(insert_endpoint_status, endpointItems.getIpAddress(), item.getItemId(), item.getItemName(), item.getItemType(), item.getStatus());
      } catch (Exception e) {
        println("Failed to insert record: " + e.getMessage())
      }
    }
  }

  def setupConnection(HttpURLConnection conn, String userId, String password) {
    conn.setRequestProperty("Content-Type", "text/xml");
    conn.setDoOutput(true);
    conn.setRequestMethod("GET");
    String userpass = userId + ":" + password;
    String basicAuth = "Basic " + javax.xml.bind.DatatypeConverter.printBase64Binary(userpass.getBytes("UTF-8"));
    conn.setRequestProperty("Authorization", basicAuth);
  }

  def saveDataToDatabase(String ipAddress, String serverType, itemList) {
    endpointItems = new EndpointItems(ipAddress, itemList)
    def sql = connectPostgreDatabse()
    insertOrUpdateEndpointStatus(sql, endpointItems)
    insertOrUpdateEndpointMeta(sql, ipAddress, serverType);
    sql.close()
    println("Save data to database successfully ")
  }

  def insertOrUpdateEndpointMeta(def sql, String ipAddress, String serverType) {
    try {
      sql.execute(insert_endpoint_meta, [ip:ipAddress, etype:serverType])
    } catch (Exception e) {
	  println("ERROR: " + e.getMessage())
    }
  }
}

