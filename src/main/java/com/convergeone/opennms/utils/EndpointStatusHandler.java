package com.convergeone.opennms.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

public class EndpointStatusHandler extends DefaultHandler {
    private List<ItemStatus> alItems = null;
    private Set<ItemStatus> alItemsSet = null;
    boolean isCamera = false;
    boolean isH323 = false;
    boolean isGateKeeper = false;
    boolean isMode = false;
    boolean isPeripherals = false;
    boolean isConnectedDevice = false;
    boolean isSIP = false;
    boolean isSIPRegistration = false;
    boolean isCallForward = false;
    boolean isProxy = false;
    boolean isVideo = false;
    boolean isVideoInput = false;
    boolean isDVI = false;
    boolean isConnector = false;
    boolean isHDMI = false;
    boolean isContactInfo = false;
    boolean isContactName = false;
    boolean isTemperature = false;
    boolean isTempThreshold = false;
    boolean isTimezone = false;
    boolean isEthernet = false;
    boolean isSpeed = false;
    private Stack<String> elementStack = null;
    private Stack<ItemStatus> objectStack = null;
    String itemId;


    EndpointStatusHandler() {
        super();
    }

    public List<ItemStatus> getAlItems() {
        Collections.sort(alItems);
        return alItems;
    }

    public void startDocument() throws SAXException {
        alItemsSet = new HashSet<>();
        elementStack = new Stack<>();
        objectStack = new Stack<>();
    }

    public void endDocument() throws SAXException {
        alItems = new ArrayList<>(alItemsSet);
        elementStack = null;
        objectStack = null;
        alItemsSet = null;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.elementStack.push(qName);
        if (qName.equalsIgnoreCase("Camera")) {
            pushItemStatus("CA", attributes);
            isCamera = true;
        } else if (("H323").equalsIgnoreCase(qName)) {
            isH323 = true;
        } else if ("GateKeeper".equalsIgnoreCase(qName)) {
            pushItemStatus("H3_GK", attributes);
            isGateKeeper = true;
        } else if ("Mode".equalsIgnoreCase(qName)) {
            pushItemStatus("H3_MD", attributes);
            isMode = true;
        } else if (qName.equalsIgnoreCase("Ethernet")) {
            pushItemStatus("ET", attributes);
            isEthernet = true;
        } else if (qName.equalsIgnoreCase("Speed") && isEthernet) {
            isSpeed = true;
        } else if ("Peripherals".equalsIgnoreCase(qName)) {
            isPeripherals = true;
        } else if (isPeripherals && "ConnectedDevice".equalsIgnoreCase(qName)) {
            pushItemStatus("PH_1_CD", attributes);
            isConnectedDevice = true;
        } else if ("CallForward".equalsIgnoreCase(qName)) {
            pushItemStatus("SP_1_CF", attributes);
            isCallForward = true;
        } else if ("Proxy".equalsIgnoreCase(qName)) {
            pushItemStatus("SP_1_PX", attributes);
            isProxy = true;
        } else if ("Registration".equalsIgnoreCase(qName)) {
            pushItemStatus("SP_1_RG", attributes);
            isSIPRegistration = true;
        } else if ("Video".equalsIgnoreCase(qName)) {
            isVideo = true;
        } else if ("Input".equalsIgnoreCase(qName) && isVideo) {
            isVideoInput = true;
        }
        if ("Connector".equalsIgnoreCase(qName) && isVideo) {
            pushItemStatus("VD_IN", attributes);
            isConnector = true;
        } else if ("DVI".equalsIgnoreCase(qName) && isVideo) {
            pushItemStatus("VD_IN_DVI", attributes);
            isDVI = true;
        } else if ("HDMI".equalsIgnoreCase(qName) && isVideo) {
            pushItemStatus("VD_IN_HDMI", attributes);
            isHDMI = true;
        } else if ("ContactInfo".equalsIgnoreCase(qName)) {
            isContactInfo = true;
        } else if ("ContactName".equalsIgnoreCase(qName)) {
            pushItemStatus("SY", attributes);
            isContactName = true;
        } else if ("Temperature".equalsIgnoreCase(qName)) {
            pushItemStatus("TEMP", attributes);
            isTemperature = true;
        } else if ("ZoneOlson".equalsIgnoreCase(qName)) {
            pushItemStatus("TZ", attributes);
            isTimezone = true;
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (isCamera) {
            setCameraStatus(ch, start, length);
        } else if (isH323 && isGateKeeper) {
            setGateKeeperStatus(ch, start, length);
        } else if (isH323 && isMode) {
            setModeStatus(ch, start, length);
        } else if (isSpeed) {
            setEthnetStatus(ch, start, length);
        } else if (isPeripherals && isConnectedDevice) {
            setPeripheralConnectedDevicesStatus(ch, start, length);
        } else if (isCallForward) {
            setCallForwardModeStatus(ch, start, length);
        } else if (isProxy) {
            setCallProxyStatus(ch, start, length);
        } else if (isSIPRegistration) {
            setSIPRegistrationStatus(ch, start, length);
        } else if (isVideoInput) {
            setVideoInputStatus(ch, start, length);
        }
        if (isConnector) {
            setVideoInputStatus(ch, start, length);
        } else if (isDVI) {
            setVideoInputDVIStatus(ch, start, length);
        } else if (isHDMI) {
            setVideoInputHDMIStatus(ch, start, length);
        } else if (isContactInfo) {
            setSystemName(ch, start, length);
        } else if (isContactName) {
            setContactName(ch, start, length);
        } else if (isTemperature) {
            setDeviceTemperatureStatus(ch, start, length);
        }  else if (isTimezone) {
            setDeviceTimeZoneSetting(ch, start, length);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.elementStack.pop();
        if (("Camera").equalsIgnoreCase(qName) && isCamera) {
            popItemStatus();
            isCamera = false;
        } else if (("GateKeeper").equalsIgnoreCase(qName)) {
            popItemStatus();
            isGateKeeper = false;
        } else if (("Mode").equalsIgnoreCase(qName)) {
            popItemStatus();
            isMode = false;
        } else if (("H323").equalsIgnoreCase(qName)) {
            isH323 = false;
        } else if (qName.equalsIgnoreCase("Ethernet")) {
            popItemStatus();
            isEthernet = false;
            isSpeed = false;
        } else if ("ConnectedDevice".equalsIgnoreCase(qName) && isPeripherals && isConnectedDevice) {
            popItemStatus();
            isConnectedDevice = false;
        } else if (("Peripherals").equalsIgnoreCase(qName)) {
            isPeripherals = false;
        } else if (("CallForward").equalsIgnoreCase(qName)) {
            popItemStatus();
            isCallForward = false;
        } else if (("Proxy").equalsIgnoreCase(qName)) {
            popItemStatus();
            isProxy = false;
        } else if (("Registration").equalsIgnoreCase(qName)) {
            popItemStatus();
            isSIPRegistration = false;
        }
        if ((isConnector && isVideoInput) && "Connector".equalsIgnoreCase(qName)) {
            popItemStatus();
            isConnector = false;
        } else if (isDVI && isVideoInput && "DVI".equalsIgnoreCase(qName)) {
            popItemStatus();
            isDVI = false;
        } else if (isHDMI && isVideoInput && "HDMI".equalsIgnoreCase(qName)) {
            popItemStatus();
            isHDMI = false;
        } else if ("Input".equalsIgnoreCase(qName)) {
            isVideoInput = false;
        } else if ("Video".equalsIgnoreCase(qName)) {
            isVideo = false;
        } else if ("ContactInfo".equalsIgnoreCase(qName)) {
            isContactInfo = false;
        } else if ("ContactName".equalsIgnoreCase(qName)) {
            popItemStatus();
            isContactName = false;
        } else if ("Temperature".equalsIgnoreCase(qName)) {
            popItemStatus();
            isTemperature = false;
        }  else if ("ZoneOlson".equalsIgnoreCase(qName)) {
            popItemStatus();
            isTimezone = false;
        }
    }

    private void setCameraStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if ("Connected".equalsIgnoreCase(currentElement()) && isCamera) {
                peekItemStatus("Camera", "Connected", new String(ch, start, length).trim());
            }
        }
    }

    private void setGateKeeperStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if ("Status".equalsIgnoreCase(currentElement())) {
                peekItemStatus("H323", "GateKeeper", new String(ch, start, length).trim());
            }
        }
    }

    private void setModeStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if ("Status".equalsIgnoreCase(currentElement())) {
                peekItemStatus("H323", "Mode", new String(ch, start, length).trim());
            }
        }
    }

    private void setPeripheralConnectedDevicesStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if (!this.objectStack.isEmpty()) {
                ItemStatus peripheralDevice = this.objectStack.peek();
                if (isPeripherals && "Name".equalsIgnoreCase(currentElement())) {
                    peripheralDevice.setItemName("Peripheral");
                } else if (isPeripherals && "Status".equalsIgnoreCase(currentElement())) {
                    peripheralDevice.setStatus(new String(ch, start, length).trim());
                } else if (isPeripherals && "Type".equalsIgnoreCase(currentElement())) {
                    peripheralDevice.setItemType(new String(ch, start, length).trim());
                }
            }
        }
    }

    private void setCallForwardModeStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if (!this.objectStack.isEmpty()) {
                ItemStatus callForward = this.objectStack.peek();
                if (!callForward.getItemId().contains("SP_1_CF")) {
                    callForward.setItemId("SP_1_CF");
                }
            }
            if ("Mode".equalsIgnoreCase(currentElement())) {
                peekItemStatus("SIP", "CallForward", new String(ch, start, length).trim());
            }
        }
    }

    private void setCallProxyStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if ("Status".equalsIgnoreCase(currentElement())) {
                peekItemStatus("SIP", "Proxy", new String(ch, start, length).trim());
            }
        }
    }

    private void setSIPRegistrationStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if (isSIPRegistration) {
                if ("Status".equalsIgnoreCase(currentElement())) {
                    if (!this.objectStack.isEmpty()) {
                        ItemStatus sipRegistration = this.objectStack.peek();
                        sipRegistration.setItemName("SIP");
                        sipRegistration.setItemType("Registration");
                        sipRegistration.setStatus(new String(ch, start, length).trim());
                    }
                }
            }
        }
    }

    private void setVideoInputStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            ItemStatus videoInput = null;
            if (isConnector && "Connected".equalsIgnoreCase(currentElement())) {
                if (!this.objectStack.isEmpty()) {
                    videoInput = this.objectStack.peek();
                    videoInput.setItemName("Video Input");
                    videoInput.setStatus(new String(ch, start, length).trim());
                }
            } else if (isConnector && "Type".equalsIgnoreCase(currentElement())) {
                if (!this.objectStack.isEmpty()) {
                    videoInput = this.objectStack.peek();
                    videoInput.setItemType(new String(ch, start, length).trim());
                }
            }
        }
    }

    private void setVideoInputDVIStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if ("Connected".equalsIgnoreCase(currentElement()) && isDVI) {
                peekItemStatus("Video Input", "DVI", new String(ch, start, length).trim());
            }
        }
    }

    private void setVideoInputHDMIStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if ("Connected".equalsIgnoreCase(currentElement()) && isHDMI) {
                peekItemStatus("Video Input", "HDMI", new String(ch, start, length).trim());
            }
        }
    }

    private void setSystemName(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if ("Name".equalsIgnoreCase(currentElement())) {
                ItemStatus itemStatus = new ItemStatus();
                itemStatus.setItemId("SY");
                itemStatus.setItemName("SystemUnit");
                itemStatus.setItemType("SystemName");
                itemStatus.setStatus(new String(ch, start, length).trim());
                alItemsSet.add(itemStatus);
            }
        }
    }

    private void setContactName(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            peekItemStatus("SystemUnit", "SystemName", new String(ch, start, length).trim());
        }
    }

    private void setDeviceTemperatureStatus(char ch[], int start, int length) throws SAXException {

        if (hasStatus(ch, start, length)) {
            String cdegreeStr =  new String(ch, start, length).trim();
            String fdegree = convertCelsiusToFahrenheit(Double.parseDouble(cdegreeStr));
            peekItemStatus("Temperature", "Temperature", fdegree);
        }
    }

    private void setDeviceTimeZoneSetting(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            peekItemStatus("TimeZone", "ZoneOlson", new String(ch, start, length).trim());
        }
    }

    private void setEthnetStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            peekItemStatus("ethernet", "speed", new String(ch, start, length).trim());

        }
    }

    private void pushItemStatus(String itemId, Attributes attributes) {
        ItemStatus itemStatus = new ItemStatus();
        itemStatus.setItemId(itemId + getItemId(attributes));
        this.objectStack.push(itemStatus);
    }

    private void popItemStatus() {
        ItemStatus itemStatus = null;
        try {
            if (!this.objectStack.isEmpty()) {
                itemStatus = this.objectStack.pop();
                if (itemStatus.getItemName() != null && itemStatus.getStatus() != null) {
                    alItemsSet.add(itemStatus);
                }
            }
        } catch (EmptyStackException e) {
        }
    }

    private void peekItemStatus(String itemName, String itemType, String status) {
        if (!this.objectStack.isEmpty()) {
            ItemStatus itemStatus = this.objectStack.peek();
            itemStatus.setItemName(itemName);
            itemStatus.setItemType(itemType);
            itemStatus.setStatus(status);
        }
    }

    private boolean hasStatus(char ch[], int start, int length) {
        return new String(ch, start, length).trim().length() > 0;
    }

    private String currentElement() {
        return this.elementStack.peek();
    }

    private String getItemId(Attributes attribute) {
        itemId = "";
        if (attribute != null && attribute.getLength() > 0) {
            if (!attribute.getValue("item").equals("null") && attribute.getValue("item") != null) {
                itemId = "_" + attribute.getValue("item");
            }
        }
        return itemId;
    }
    private String convertCelsiusToFahrenheit(double celsius ){
        double fDegree = celsius * 1.8 + 32;
        return  String.valueOf(fDegree);
    }
}

