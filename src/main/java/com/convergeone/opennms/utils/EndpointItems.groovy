package com.convergeone.opennms.utils

class EndpointItems {
  private static final long serialVersionUID = 46869984017416527L;
  private String ipAddress
  private List<ItemStatus> alItems = null

  EndpointItems(String ipAddress, List<ItemStatus> items) {
    setIpAddress(ipAddress)
    setAlItems(items)

  }

  String getIpAddress() {
    return ipAddress
  }

  void setIpAddress(String ipAddress) {
    this.ipAddress = ipAddress
  }

  List<ItemStatus> getAlItems() {
    return alItems
  }

  void setAlItems(List<ItemStatus> alItems) {
    this.alItems = alItems
  }

  @Override
  public String toString() {
    return "EndpointItems{" +
      "ipAddress='" + ipAddress + '\'' +
      ", alItems=" + alItems +
      '}';
  }
}
