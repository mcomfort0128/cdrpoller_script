package com.convergeone.opennms.utils;

public class FieldData {
    private static final long serialVersionUID = 46869417416527L;
    private String itemId;
    private String itemName;
    private String itemType;

    public FieldData(String itemId, String itemName, String itemType) {
        this.itemId = itemId;
        this.itemName = itemName;
        this.itemType = itemType;
    }

    public String getItemId() {
        return itemId;
    }

    public String getItemName() {
        return itemName;
    }

    public String getItemType() {
        return itemType;
    }
}
