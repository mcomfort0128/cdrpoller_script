package com.convergeone.opennms.utils

import javax.net.ssl.*
import java.security.SecureRandom
import java.security.Security
import java.security.cert.X509Certificate
import java.text.SimpleDateFormat
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@GrabConfig(systemClassLoader = true)

class CDRPollerWithExecutorService {
  private final int MYTHREADS = 30;
  private final String prop_path = "/opt/onguard/etc/auditProperties.xml";
  private final String query = "<Command><CallHistory><Get><DetailLevel>Full</DetailLevel></Get></CallHistory></Command>"
  private final String tandbergmxp_url = "https://10.244.24.85/history.xml"
  private Utils utils = new Utils()


  static {
    Security.setProperty("jdk.tls.disabledAlgorithms", "");
  }

  public CDRPollerWithExecutorService() {
  }

  def executorService() {
    ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
    List<NodeInfo> nodeInfos = utils.getNodeInfo(Constants.TELEPRESENCE_CDR);
    if(!nodeInfos.isEmpty() && nodeInfos.size() > 0) {
      try {
        for (NodeInfo node : nodeInfos) {
          Runnable worker = new MyRunnable(node);
          executor.execute(worker);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
        println("Successfully download CDRs from endpoints")
      } catch (Exception e) {
        System.out.println("Error with request: " + e.getMessage())
      }
    } else {
      println("No IP address to make request ")
    }
  }

  def getCDRsFromEndpoint(String ip, String userId, String password, String serverType) {
    String url = "http://" + ip + "/putxml"
    try {
      URL obj = new URL(url);
      HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
      utils.setupConnection(conn, userId, password);
      conn.setRequestMethod("POST");
      conn.setConnectTimeout(60000);
      String data = query;
      OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
      out.write(data);
      out.close();
      int responseCode = conn.getResponseCode()
      if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
        println("IP Address: " + ip + " URL: " + url + " - Page not found!")
      } else if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
        createHTTPsConnection(ip, userId, password, serverType)
      } else {
        InputStreamReader input = new InputStreamReader(conn.getInputStream())
        String path = "/var/opt/onguard/cdr/" + serverType + "/"
        writeInputStreamToFile(input, path)
        input.close();
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  def createHTTPsConnection(String ipAddress, String userId, String password, String serverType) throws Exception {
    boolean isTanbergMxp = false;
    try {
      def trustAllCerts = [
        new X509TrustManager() {
          public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null }

          public void checkClientTrusted(X509Certificate[] certs, String authType) {}

          public void checkServerTrusted(X509Certificate[] certs, String authType) {}
        }
      ] as TrustManager[]

      // Install the all-trusting trust manager

      SSLContext sc = SSLContext.getInstance("TLS");
      sc.init(null, trustAllCerts, new SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

      // Create all-trusting host name verifier
      HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      };
      HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

      String url = "https://" + ipAddress + "/putxml"
      if (ipAddress.equalsIgnoreCase("10.244.24.85")) {
        url = tandbergmxp_url
        isTanbergMxp = true;
      }

      URL obj = new URL(url);
      HttpsURLConnection connHTTPs = (HttpsURLConnection) obj.openConnection();
      connHTTPs.setConnectTimeout(60000);
      utils.setupConnection(connHTTPs, userId, password);

      if (isTanbergMxp) {
        connHTTPs.setRequestMethod("GET");
      } else {
        connHTTPs.setRequestMethod("POST");
        OutputStreamWriter out = new OutputStreamWriter(connHTTPs.getOutputStream());
        out.write(query);
        out.close();
      }

      InputStreamReader input = new InputStreamReader(connHTTPs.getInputStream())
      String path = "/var/opt/onguard/cdr/" + serverType + "/"
      writeInputStreamToFile(input, path)
      input.close();
    } catch (Exception e) {
    }
  }

  def writeInputStreamToFile(InputStreamReader input, String path) {
    def rootDir = new File(path);
    if (!rootDir.exists()) {
      rootDir.mkdirs();
      rootDir.setExecutable(true)
      rootDir.setReadable(true)
      rootDir.setWritable(true)
    }

    String name = rootDir.path + "/" + "CDR_" + getNowCDT() + ".json"
    def targetFile = new File(name)
    OutputStream outputStream = new FileOutputStream(targetFile);
    Writer outputStreamWriter = new OutputStreamWriter(outputStream, "UTF-8");
    String inputLine;
    def xml = ''
    while ((inputLine = input.readLine()) != null) {
      xml = xml + inputLine
    }
    outputStreamWriter.write(toJsonBuilder(xml).toPrettyString())
    outputStreamWriter.close()
    println("Run with ExecutorService: " + name)
  }

  def toJsonBuilder(xml) {
    def pojo = build(new XmlParser().parseText(xml))
    new groovy.json.JsonBuilder(pojo)
  }

  def build(node) {
    if (node instanceof String) {
      return
    }
    def map = ['name': node.name()]
    if (!node.attributes().isEmpty()) {
      map.put('attributes', node.attributes().collectEntries { it })
    }
    if (!node.children().isEmpty() && !(node.children().get(0) instanceof String)) {
      map.put('children', node.children().collect { build(it) }.findAll { it != null })
    } else if (node.text() != '') {
      map.put('value', node.text())
    }
    map
  }

  def getNowCDT() {
    SimpleDateFormat sdfDate = new SimpleDateFormat("MM-dd-yyyy-@-HH-mm-ss")
    sdfDate.setTimeZone(TimeZone.getTimeZone("EST"))
    Date now = new Date()
    String strDate = sdfDate.format(now)
    return strDate;
  }


  class MyRunnable implements Runnable {
    private final NodeInfo nodeInfo;

    MyRunnable() {}

    MyRunnable(NodeInfo node) {
      this.nodeInfo = node;
    }

    @Override
    public void run() {
      if (!("".equals(nodeInfo.userId) && nodeInfo.userId != null) && (!"".equals(nodeInfo.password) && nodeInfo.password != null)) {
        getCDRsFromEndpoint(nodeInfo.ip, nodeInfo.userId, nodeInfo.password, nodeInfo.serverType)
      } else {
        println("IP: " + nodeInfo.ip + " has no login credential")
      }
    }
  }
}