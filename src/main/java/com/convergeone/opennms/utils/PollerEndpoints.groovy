package com.convergeone.opennms.utils

import javax.net.ssl.*
import javax.xml.parsers.SAXParser
import javax.xml.parsers.SAXParserFactory
import java.security.SecureRandom
import java.security.Security
import java.security.cert.X509Certificate

@GrabConfig(systemClassLoader = true)

class PollerEndpoints {
  private final String status_url = "/getxml?location=/Status"
  private final String tandbergmxp_url = "https://10.244.24.85/status.xml"
  private final String http = "http://"
  private final String https = "https://"
  private EndpointStatusHandler endpointStatusHandler = null
  private HTTPsEndpointStatusHandler httPsEndpointStatusHandler = null
  private List<ItemStatus> itemList = null;
  private Utils utils = new Utils()
  static {
    Security.setProperty("jdk.tls.disabledAlgorithms", "");
  }

  PollerEndpoints() {}

  def getMonitorDataFromEndpoint() {
    List<NodeInfo> nodeInfos = utils.getNodeInfo(Constants.TELEPRESENCE_ENDPOINT);
    if (!nodeInfos.isEmpty() && nodeInfos.size() > 0) {
      for (NodeInfo nodeInfo : nodeInfos) {
        sendGetRequestToEndpoint(nodeInfo.getIp(), nodeInfo.getUserId(), nodeInfo.getPassword(), nodeInfo.getServerType())
      }
    } else {
      println("No IP address to make request ")
    }
  }

  def sendGetRequestToEndpoint(String ipAddress, String userId, String password, String serverType) throws Exception {
    final String url = http + ipAddress + status_url;
    try {
      URL obj = new URL(url);
      HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
      conn.setConnectTimeout(60000);
      utils.setupConnection(conn, userId, password)
      int responseCode = conn.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        try {
          println("IP Address: " + ipAddress + " URL: " + url)
          filterDataFromXMLResponse(conn.getInputStream())
          utils.saveDataToDatabase(ipAddress, serverType, itemList)
        } catch (Exception e) {
          e.printStackTrace();
        }
      } else if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP) {
        sendHttpsGetRequest(ipAddress, userId, password, serverType)
      } else if (responseCode == HttpURLConnection.HTTP_NOT_FOUND) {
        println("IP Address: " + ipAddress + " URL: " + url + " - Page not found!")
      } else {
        println("No response from endpoint: " + ipAddress)
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  def sendHttpsGetRequest(String ipAddress, String userId, String password, String serverType) throws Exception {
    String url = https + ipAddress + status_url;
    if ("10.244.24.85".equals(ipAddress)) {
      url = tandbergmxp_url
    }

    try {
      def trustAllCerts = [
        new X509TrustManager() {
          public java.security.cert.X509Certificate[] getAcceptedIssuers() { return null }

          public void checkClientTrusted(X509Certificate[] certs, String authType) {}

          public void checkServerTrusted(X509Certificate[] certs, String authType) {}
        }
      ] as TrustManager[]

      // Install the all-trusting trust manager
      SSLContext sc = SSLContext.getInstance("TLS");
      sc.init(null, trustAllCerts, new SecureRandom());
      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());

      // Create all-trusting host name verifier
      HostnameVerifier allHostsValid = new HostnameVerifier() {
        public boolean verify(String hostname, SSLSession session) {
          return true;
        }
      };
      HttpsURLConnection.setDefaultHostnameVerifier(allHostsValid);

      URL obj = new URL(url);
      HttpsURLConnection conn = (HttpsURLConnection) obj.openConnection();
      conn.setConnectTimeout(60000);
      utils.setupConnection(conn, userId, password)
      int responseCode = conn.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_OK) {
        try {
          println("IP Address: " + ipAddress + " URL: " + url)
          if (url.indexOf(".xml") > 0) {
            filterHttpsDataFromXMLResponse(conn.getInputStream())
          } else {
            filterDataFromXMLResponse(conn.getInputStream())
          }
          utils.saveDataToDatabase(ipAddress, serverType, itemList)
        } catch (Exception e) {
          e.printStackTrace();
        }
      } else {
        System.out.println("GET request not worked");
      }
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  def filterHttpsDataFromXMLResponse(InputStream inputStream) {
    try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();
      httPsEndpointStatusHandler = new HTTPsEndpointStatusHandler();
      saxParser.parse(inputStream, httPsEndpointStatusHandler);
      itemList = httPsEndpointStatusHandler.getAlItems();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  def filterDataFromXMLResponse(InputStream inputStream) {
    try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();
      endpointStatusHandler = new EndpointStatusHandler();
      saxParser.parse(inputStream, endpointStatusHandler);
      itemList = endpointStatusHandler.getAlItems();
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  def printItemList() {
    for (ItemStatus itemStatus : itemList) {
      println("Item Id: " + itemStatus.getItemId())
      println("Item Name: " + itemStatus.getItemName())
      println("Item Type: " + itemStatus.getItemType())
      println("Item Status: " + itemStatus.getStatus())
    }
  }
}

