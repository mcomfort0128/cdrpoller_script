import com.convergeone.opennms.utils.CDRPollerWithExecutorService
import com.convergeone.opennms.utils.McuXMLPoller
import com.convergeone.opennms.utils.PollerEndpoints

CDRPollerWithExecutorService cdrPollerWithExecutorService = new CDRPollerWithExecutorService()
try {
  cdrPollerWithExecutorService.executorService()
} catch (Exception ex) {
  println("Can't call method executorService. Error: " + ex.getMessage())
  throw new Exception("Skip getting CDR frm endpoint")
}

PollerEndpoints pollerEndpoints = new PollerEndpoints()
try {
  pollerEndpoints.getMonitorDataFromEndpoint()
} catch (Exception ex) {
  println("Can't call method getMonitorDataFromEndpoint. Error: " + ex.getMessage())
  throw new Exception("Skip getting Monitor Data from endpoint")
}

McuXMLPoller app = new McuXMLPoller()
try {
  app.executorService()
} catch (Exception ex) {
  println("Can't call method executorService. Error: " + ex.getMessage())
  throw new Exception("Skip getting Monitor Data from MCU")
}

