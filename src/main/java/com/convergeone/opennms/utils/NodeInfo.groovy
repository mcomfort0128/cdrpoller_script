package com.convergeone.opennms.utils

class NodeInfo {
  String ip;
  String userId;
  String password;
  String serverType;

  NodeInfo(String ip, String userId, String password, String serverType) {
    this.ip = ip;
    this.userId = userId;
    this.password = password;
    this.serverType = serverType;
  }

  boolean equals(o) {
    if (this.is(o)) return true
    if (!(o instanceof NodeInfo)) return false

    NodeInfo nodeInfo = (NodeInfo) o

    if (ip != nodeInfo.ip) return false
    if (password != nodeInfo.password) return false
    if (serverType != nodeInfo.serverType) return false
    if (userId != nodeInfo.userId) return false

    return true
  }

  int hashCode() {
    int result
    result = (ip != null ? ip.hashCode() : 0)
    result = 31 * result + (userId != null ? userId.hashCode() : 0)
    result = 31 * result + (password != null ? password.hashCode() : 0)
    result = 31 * result + (serverType != null ? serverType.hashCode() : 0)
    return result
  }
}

