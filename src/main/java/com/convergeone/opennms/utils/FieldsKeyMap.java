package com.convergeone.opennms.utils;

import java.util.HashMap;
import java.util.Map;


public class FieldsKeyMap {
    private Map<String, FieldData> map = null;

    public FieldsKeyMap(String queryType) {
        if (queryType.equalsIgnoreCase(Constants.DEVICE_HEALTH_QUERY)) {
            createDeviceHealthMap();
        } else if (queryType.equalsIgnoreCase(Constants.DEVICE_QUERY)) {
            createDevicehMap();
        } else if (queryType.equalsIgnoreCase(Constants.SIP_QUERY)) {
            createSipMap();
        }
    }

    /**
     * @return the map
     */
    public Map<String, FieldData> getMap() {
        return map;
    }

    private Map<String, FieldData> createDeviceHealthMap() {
        map = new HashMap<>();
        map.put(Constants.CPU_LOAD, new FieldData(Constants.CPU_LOAD_CODE, Constants.CPU_LOAD_NAME, Constants.CPU_LOAD_TYPE));
        map.put(Constants.MEDIA_LOAD, new FieldData(Constants.MEDIA_LOAD_CODE, Constants.MEDIA_LOAD_NAME, Constants.MEDIA_LOAD_TYPE));
        map.put(Constants.AUDIO_LOAD, new FieldData(Constants.AUDIO_LOAD_CODE, Constants.AUDIO_LOAD_NAME, Constants.AUDIO_LOAD_TYPE));
        map.put(Constants.VIDEO_LOAD, new FieldData(Constants.VIDEO_LOAD_CODE, Constants.VIDEO_LOAD_NAME, Constants.VIDEO_LOAD_TYPE));
        map.put(Constants.FAN_STATUS,new FieldData(Constants.FAN_STATUS_CODE, Constants.FAN_STATUS_NAME, Constants.FAN_STATUS_TYPE));
        map.put(Constants.FAN_STATUS_WORST, new FieldData(Constants.FAN_STATUS_WORST_CODE, Constants.FAN_STATUS_WORST_NAME, Constants.FAN_STATUS_WORST_TYPE));
        map.put(Constants.TEMPERATURE_STATUS, new FieldData(Constants.TEMPERATURE_STATUS_CODE, Constants.TEMPERATURE_STATUS_NAME, Constants.TEMPERATURE_STATUS_TYPE));
        map.put(Constants.TEMPERATURE_STATUS_WORST, new FieldData(Constants.TEMPERATURE_STATUS_WORST_CODE, Constants.TEMPERATURE_STATUS_WORST_NAME, Constants.TEMPERATURE_STATUS_WORST_TYPE));
        map.put(Constants.BATTERY_STATUS,new FieldData(Constants.BATTERY_STATUS_CODE, Constants.BATTERY_STATUS_NAME, Constants.BATTERY_STATUS_TYPE));
        map.put(Constants.BATTERY_STATUS_WOSRT, new FieldData(Constants.BATTERY_STATUS_WOSRT_CODE, Constants.BATTERY_STATUS_WOSRT_NAME, Constants.BATTERY_STATUS_WOSRT_TYPE));
        map.put(Constants.VOLTAGE_STATUS, new FieldData(Constants.VOLTAGE_STATUS_CODE, Constants.VOLTAGE_STATUS_NAME, Constants.VOLTAGE_STATUS_TYPE));
        map.put(Constants.VOLTAGE_STATUS_WORST, new FieldData(Constants.VOLTAGE_STATUS_WORST_CODE, Constants.VOLTAGE_STATUS_WORST_NAME, Constants.VOLTAGE_STATUS_WORST_TYPE));
        map.put(Constants.OPERATIONAL_STATUS, new FieldData(Constants.OPERATIONAL_STATUS_CODE, Constants.OPERATIONAL_STATUS_NAME, Constants.OPERATIONAL_STATUS_TYPE));
        return map;
    }

    private Map<String, FieldData> createDevicehMap() {
        map = new HashMap<>();
        map.put(Constants.CURRENT_TIME,  new FieldData(Constants.CURRENT_TIME_CODE, Constants.CURRENT_TIME_NAME, Constants.CURRENT_TIME_TYPE));
        map.put(Constants.RESTART_TIME, new FieldData(Constants.RESTART_TIME_CODE, Constants.RESTART_TIME_NAME, Constants.RESTART_TIME_TYPE));
        map.put(Constants.SERIAL, new FieldData(Constants.SERIAL_CODE, Constants.SERIAL_NAME, Constants.SERIAL_TYPE));
        map.put(Constants.SOFTWARE_VERSION,  new FieldData(Constants.SOFTWARE_VERSION_CODE, Constants.SOFTWARE_VERSION_NAME, Constants.SOFTWARE_VERSION_TYPE));
        map.put(Constants.BUILD_VERSION,  new FieldData(Constants.BUILD_VERSION_CODE, Constants.BUILD_VERSION_NAME, Constants.BUILD_VERSION_TYPE));
        map.put(Constants.MODEL,  new FieldData(Constants.MODEL_CODE, Constants.MODEL_NAME, Constants.MODEL_TYPE));
        map.put(Constants.API_VERSION,  new FieldData(Constants.API_VERSION_CODE, Constants.API_VERSION_NAME, Constants.API_VERSION_TYPE));
        map.put(Constants. ACTIVATE_FEATURES, new FieldData(Constants.ACTIVATE_FEATURES_CODE, Constants.ACTIVATE_FEATURES_NAME, Constants.ACTIVATE_FEATURES_TYPE));
        map.put(Constants. ACTIVATE_LICENCES,  new FieldData(Constants.ACTIVATE_LICENCES_CODE, Constants.ACTIVATE_LICENCES_NAME, Constants.ACTIVATE_LICENCES_TYPE));
        map.put(Constants.LICENCES, new FieldData(Constants.LICENCES_CODE, Constants.LICENCES_NAME, Constants.LICENCES_TYPE));
        map.put(Constants.PORTS,  new FieldData(Constants.PORTS_CODE, Constants.PORTS_NAME, Constants.PORTS_TYPE));
        map.put(Constants.EXPIRY,  new FieldData(Constants.EXPIRY_CODE, Constants.EXPIRY_NAME, Constants.EXPIRY_TYPE));
        map.put(Constants.CLUSTER_TYPE,  new FieldData(Constants.CLUSTER_TYPE_CODE, Constants.CLUSTER_TYPE_NAME, Constants.CLUSTER_TYPE_TYPE));
        map.put(Constants.MAX_CONFERENCE_SIZE,  new FieldData(Constants.MAX_CONFERENCE_SIZE_CODE, Constants.MAX_CONFERENCE_SIZE_NAME, Constants.MAX_CONFERENCE_SIZE_TYPE));
        map.put(Constants.TOTAL_VIDEO_PORTS,  new FieldData(Constants.TOTAL_VIDEO_PORTS_CODE, Constants.TOTAL_VIDEO_PORTS_NAME, Constants.TOTAL_VIDEO_PORTS_TYPE));
        map.put(Constants.TOTAL_AUDIO_ONLY_PORTS,  new FieldData(Constants.TOTAL_AUDIO_ONLY_PORTS_CODE, Constants.TOTAL_AUDIO_ONLY_PORTS_NAME, Constants.TOTAL_AUDIO_ONLY_PORTS_TYPE));
        map.put(Constants.TOTAL_STREAMING_CONTENT_PORTS,  new FieldData(Constants.TOTAL_STREAMING_CONTENT_PORTS_CODE, Constants.TOTAL_STREAMING_CONTENT_PORTS_NAME, Constants.TOTAL_STREAMING_CONTENT_PORTS_TYPE));
        map.put(Constants.PORT_RESERVATION_MODE, new FieldData(Constants.PORT_RESERVATION_MODE_CODE, Constants.PORT_RESERVATION_MODE_NAME, Constants.PORT_RESERVATION_MODE_TYPE));
        map.put(Constants.MAX_VIDEO_SOLUTION,  new FieldData(Constants.MAX_VIDEO_SOLUTION_CODE, Constants.MAX_VIDEO_SOLUTION_NAME, Constants.MAX_VIDEO_SOLUTION_TYPE));
        map.put(Constants.VIDEO_PORT_ALLOCATION,  new FieldData(Constants.VIDEO_PORT_ALLOCATION_CODE, Constants.VIDEO_PORT_ALLOCATION_NAME, Constants.VIDEO_PORT_ALLOCATION_TYPE));
        map.put(Constants.SHUTDOWN_STATUS,  new FieldData(Constants.SHUTDOWN_STATUS_CODE, Constants.SHUTDOWN_STATUS_NAME, Constants.SHUTDOWN_STATUS_TYPE));
        map.put(Constants.REBOOT_REQUIRED,  new FieldData(Constants.REBOOT_REQUIRED_CODE, Constants.REBOOT_REQUIRED_NAME, Constants.REBOOT_REQUIRED_TYPE));
        map.put(Constants. FINISHED_BOOTING,  new FieldData(Constants.FINISHED_BOOTING_CODE, Constants.FINISHED_BOOTING_NAME, Constants.FINISHED_BOOTING_TYPE));
        map.put(Constants. MEDIA_RESOURCE,  new FieldData(Constants.MEDIA_RESOURCE_CODE, Constants.MEDIA_RESOURCE_NAME, Constants.MEDIA_RESOURCE_TYPE));
        map.put(Constants.MEDIA_RESOURCE_RESTART,  new FieldData(Constants.MEDIA_RESOURCE_RESTART_CODE, Constants.MEDIA_RESOURCE_RESTART_NAME, Constants.MEDIA_RESOURCE_RESTART_TYPE));
        return map;
    }

    private Map<String, FieldData> createSipMap() {
        map = new HashMap<>();
        map.put(Constants.OUTBOUND_CONFIGURATION, new FieldData(Constants.OUTBOUND_CONFIGURATION_CODE, Constants.OUTBOUND_CONFIGURATION_NAME, Constants.OUTBOUND_CONFIGURATION_TYPE));
        map.put(Constants.OUTBOUND_ADDRESS, new FieldData(Constants.OUTBOUND_ADDRESS_CODE, Constants.OUTBOUND_ADDRESS_NAME, Constants.OUTBOUND_ADDRESS_TYPE));
        map.put(Constants.OUTBOUND_DOMAIN, new FieldData(Constants.OUTBOUND_DOMAIN_CODE, Constants.OUTBOUND_DOMAIN_NAME, Constants.OUTBOUND_DOMAIN_TYPE));
        map.put(Constants.CONFIGURATION_REGISTRAR, new FieldData(Constants.CONFIGURATION_REGISTRAR_CODE, Constants.CONFIGURATION_REGISTRAR_NAME, Constants.CONFIGURATION_REGISTRAR_TYPE));
        map.put(Constants.CONFIGURATION_PROXY,new FieldData(Constants.CONFIGURATION_PROXY_CODE, Constants.CONFIGURATION_PROXY_NAME, Constants.CONFIGURATION_PROXY_TYPE));
        map.put(Constants.CONFIGURATION_CONTACT_URL, new FieldData(Constants.CONFIGURATION_CONTACT_URL_CODE, Constants.CONFIGURATION_CONTACT_URL_NAME, Constants.CONFIGURATION_CONTACT_URL_TYPE));
        map.put(Constants.CONFIGURATION_CONTACT_DOMAIN, new FieldData(Constants.CONFIGURATION_CONTACT_DOMAIN_CODE, Constants.CONFIGURATION_CONTACT_DOMAIN_NAME, Constants.CONFIGURATION_CONTACT_DOMAIN_TYPE));
        map.put(Constants.CONFERENCE_REGISTRATION, new FieldData(Constants.CONFERENCE_REGISTRATION_CODE, Constants.CONFERENCE_REGISTRATION_NAME, Constants.CONFERENCE_REGISTRATION_TYPE));
        map.put(Constants.REGISTRAR_UASAGE,new FieldData(Constants.REGISTRAR_USAGE_CODE, Constants.REGISTRAR_USAGE_NAME, Constants.REGISTRAR_USAGE_TYPE));
        map.put(Constants.REGISTRAR_TYPE, new FieldData(Constants.REGISTRAR_TYPE_CODE, Constants.REGISTRAR_TYPE_NAME, Constants.REGISTRAR_TYPE_TYPE));
        map.put(Constants.MAX_OCS_BITRATE, new FieldData(Constants.MAX_OCS_BITRATE_CODE, Constants.MAX_OCS_BITRATE_NAME, Constants.MAX_OCS_BITRATE_TYPE));
        map.put(Constants.OUTGOING_TRANSPORT, new FieldData(Constants.OUTGOING_TRANSPORT_CODE, Constants.OUTGOING_TRANSPORT_NAME, Constants.OUTGOING_TRANSPORT_TYPE));
        map.put(Constants.USE_LOCAL_CERTIFICATE, new FieldData(Constants.USE_LOCAL_CERTIFICATE_CODE, Constants.USE_LOCAL_CERTIFICATE_NAME, Constants.USE_LOCAL_CERTIFICATE_TYPE));
        map.put(Constants.REGISTRATION_STATUS, new FieldData(Constants.REGISTRATION_STATUS_CODE, Constants.REGISTRATION_STATUS_NAME, Constants.REGISTRATION_STATUS_TYPE));
        return map;
    }

    @Override
    public String toString() {
        return "FieldsKeyMap{" +
            "map=" + map +
            '}';
    }
}
