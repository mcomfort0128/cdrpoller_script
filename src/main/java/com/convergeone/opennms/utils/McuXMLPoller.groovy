package com.convergeone.opennms.utils

import javax.xml.parsers.SAXParser
import javax.xml.parsers.SAXParserFactory
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

@GrabConfig(systemClassLoader = true)

class McuXMLPoller {
  private final int MYTHREADS = 30;
  private final String[] mcu_mse8220_queryNames = [Constants.DEVICE_HEALTH_QUERY, Constants.DEVICE_QUERY, Constants.SIP_QUERY]
  private McuXmlHandler mcuXmlHandler = null
  private List<ItemStatus> itemList = null
  private List<ItemStatus> allItemList = null
  private EndpointItems endpointItems = null
  private Utils utils = new Utils()
  private Set<ItemStatus> alItemsSet = null;


  def executorService() {
    ExecutorService executor = Executors.newFixedThreadPool(MYTHREADS);
    List<NodeInfo> nodeInfos = utils.getNodeInfo(Constants.TELEPRESENCE_MCU);
    if (!nodeInfos.isEmpty() && nodeInfos.size() > 0) {
      try {
        for (NodeInfo node : nodeInfos) {
          Runnable worker = new MyRunnable(node);
          executor.execute(worker);
        }
        executor.shutdown();
        while (!executor.isTerminated()) {
        }
      } catch (Exception e) {
        System.out.println("Error with request: " + e.getMessage())
      }
    } else {
      println("No IP address to make request ")
    }
  }

  def xmlrpcRequests(String ip, String userId, String password, String serverType) {
    String urlEndPoint = "http://" + ip + "/rpc2"
    alItemsSet = new HashSet<>();
    mcu_mse8220_queryNames.each {
      String xmlrpcQuery = generateXMPRPCRequest(it, userId, password)
      requestQuery(urlEndPoint, xmlrpcQuery, ip, serverType, it)
    }
    allItemList = new ArrayList<>(alItemsSet);
    utils.saveDataToDatabase(ip, serverType, allItemList);
  }

  def requestQuery(String urlEndPoint, String queryName, String ipAddress, String serverType, String queryType) {
    try {
      URL obj = new URL(urlEndPoint);
      HttpURLConnection conn = (HttpURLConnection) obj.openConnection();
      conn.setDoOutput(true);
      conn.setRequestMethod("POST");
      conn.setConnectTimeout(60000);
      OutputStreamWriter out = new OutputStreamWriter(conn.getOutputStream());
      out.write(queryName);
      out.close();
      filterDataFromXMLResponse(conn.getInputStream(), queryType)
    }
    catch (Exception e) {
      e.printStackTrace();
    }
  }

  def filterDataFromXMLResponse(InputStream inputStream, String queryType) {
    try {
      SAXParserFactory factory = SAXParserFactory.newInstance();
      SAXParser saxParser = factory.newSAXParser();
      mcuXmlHandler = new McuXmlHandler(queryType);
      saxParser.parse(inputStream, mcuXmlHandler);
      itemList = mcuXmlHandler.getAlItems();
      addItemListToSet(itemList)
    } catch (Exception e) {
      e.printStackTrace();
    }
  }

  def addItemListToSet(List<ItemStatus> itemList) {
    for (ItemStatus itemStatus : itemList) {
      alItemsSet.add(itemStatus);
    }
  }

  def generateXMPRPCRequest(String queryName, String userName, String password) {
    "<?xml version='1.0' encoding='UTF-8'?>\n"
    "<methodCall>\n" +
      "\t<methodName>" + queryName + "</methodName>\n" +
      "\t<params>\n" +
      "\t\t<param>\n" +
      "\t\t\t<value>\n" +
      "\t\t\t\t<struct>\n" +
      "\t\t\t\t\t<member>\n" +
      "\t\t\t\t\t\t<name>authenticationPassword</name>\n" +
      "\t\t\t\t\t\t<value><string>" + password + "</string></value>\n" +
      "\t\t\t\t\t</member>\n" +
      "\t\t\t\t\t<member>\n" +
      "\t\t\t\t\t\t<name>authenticationUser</name>\n" +
      "\t\t\t\t\t\t<value><string>" + userName + "</string></value>\n" +
      "\t\t\t\t\t</member>\n" +
      "\t\t\t\t</struct>\n" +
      "\t\t\t</value>\n" +
      "\t\t</param>\n" +
      "\t</params>\n" +
      "</methodCall>";
  }

  def printItemList() {
    for (ItemStatus itemStatus : itemList) {
      println("--------------------ITEM LIST FROM THE HANDLER CLASS------------------")
      println("Item Id: " + itemStatus.getItemId())
      println("Item Name: " + itemStatus.getItemName())
      println("Item Type: " + itemStatus.getItemType())
      println("Item Status: " + itemStatus.getStatus())
    }
  }

  class MyRunnable implements Runnable {
    private final NodeInfo nodeInfo;

    MyRunnable(NodeInfo node) {
      this.nodeInfo = node;
    }

    @Override
    public void run() {
      if (!("".equals(nodeInfo.userId) && nodeInfo.userId != null) && (!"".equals(nodeInfo.password) && nodeInfo.password != null)) {
        xmlrpcRequests(nodeInfo.ip, nodeInfo.userId, nodeInfo.password, nodeInfo.serverType)
      } else {
        println("IP: " + nodeInfo.ip + " has no login credential")
      }
    }
  }
}