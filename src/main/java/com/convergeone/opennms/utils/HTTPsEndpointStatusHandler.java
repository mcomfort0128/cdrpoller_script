package com.convergeone.opennms.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

public class HTTPsEndpointStatusHandler extends DefaultHandler {
    private List<ItemStatus> alItems = null;
    private Set<ItemStatus> alItemsSet = null;
    boolean isCamera = false;
    boolean isGateKeeper = false;
    boolean isSIP = false;
    boolean isSIPRegistration = false;
    boolean isTemperature = false;
    boolean isEthernet = false;
    boolean isSpeed = false;
    private Stack<String> elementStack = null;
    private Stack<ItemStatus> objectStack = null;
    String itemId;
    String itemStatus;

    HTTPsEndpointStatusHandler() {
        super();
    }

    public List<ItemStatus> getAlItems() {
        Collections.sort(alItems);
        return alItems;
    }

    public void startDocument() throws SAXException {
        alItemsSet = new HashSet<>();
        elementStack = new Stack<>();
        objectStack = new Stack<>();
    }

    public void endDocument() throws SAXException {
        alItems = new ArrayList<>(alItemsSet);
        elementStack = null;
        objectStack = null;
        alItemsSet = null;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.elementStack.push(qName);
        if (qName.equalsIgnoreCase("TemperatureFahrenheit")) {
            pushItemStatus("TEMP", attributes);
            isTemperature = true;
        } else if (qName.equalsIgnoreCase("Ethernet")) {
            pushItemStatus("ET", attributes);
            isEthernet = true;
        } else if (qName.equalsIgnoreCase("Speed") && isEthernet) {
            isSpeed = true;
        } else if (qName.equalsIgnoreCase("H323Gatekeeper")) {
            pushItemStatus("H3_GK", attributes);
            getItemStatus(attributes);
            isGateKeeper = true;
        } else if (qName.equalsIgnoreCase("Camera")) {
            popItemStatus();
            pushItemStatus("CA", attributes);
            getItemConnected(attributes);
            isCamera = true;
        } else if (qName.equalsIgnoreCase("SIP")) {
            if (!this.objectStack.isEmpty()) {
                this.objectStack.pop();
            }
            isSIP = true;
        } else if (isSIP && qName.equalsIgnoreCase("Registration")) {
            pushItemStatus("SP_1_RG", attributes);
            getItemStatus(attributes);
            isSIPRegistration = true;
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (isTemperature) {
            setDeviceTemperatureStatus(ch, start, length);
        } else if (isSpeed) {
            setEthnetStatus(ch, start, length);
        } else if (isGateKeeper) {
            setGateKeeperStatus();
        } else if (isCamera) {
            setCameraStatus();
        } else if (isSIPRegistration) {
            setSIPRegistrationStatus(ch, start, length);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        this.elementStack.pop();
        if (qName.equalsIgnoreCase("SystemUnit")) {
            popItemStatus();
            isTemperature = false;
        } else if (qName.equalsIgnoreCase("Ethernet")) {
            popItemStatus();
            isEthernet = false;
            isSpeed = false;
        } else if (qName.equalsIgnoreCase("H323Gatekeeper")) {
            popItemStatus();
            isGateKeeper = false;
        } else if (qName.equalsIgnoreCase("Registration")) {
            popItemStatus();
            isSIPRegistration = false;
        } else if (qName.equalsIgnoreCase("FarEndInformation")) {
            popItemStatus();
            isCamera = false;
        }
    }

    private void setCameraStatus() throws SAXException {
        peekItemStatus("Camera", "Connected", getItemStatus());
    }

    private void setGateKeeperStatus() throws SAXException {
        peekItemStatus("H323", "GateKeeper", getItemStatus());
    }

    private void setSIPRegistrationStatus(char ch[], int start, int length) throws SAXException {
        peekItemStatus("SIP", "Registration", itemStatus);
        setContactName(ch, start, length);
    }

    private void setContactName(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            if ("URI".equalsIgnoreCase(currentElement())) {
                ItemStatus itemStatus = new ItemStatus();
                itemStatus.setItemId("SY");
                itemStatus.setItemName("SystemUnit");
                itemStatus.setItemType("SystemName");
                itemStatus.setStatus(new String(ch, start, length).trim());
                alItemsSet.add(itemStatus);
            }
        }
    }

    private void setEthnetStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            peekItemStatus("ethernet", "speed", new String(ch, start, length).trim());
        }
    }

    private void setDeviceTemperatureStatus(char ch[], int start, int length) throws SAXException {
        if (hasStatus(ch, start, length)) {
            peekItemStatus("Temperature", "Temperature", new String(ch, start, length).trim());
        }
    }


    private void pushItemStatus(String itemId, Attributes attributes) {
        ItemStatus itemStatus = new ItemStatus();
        itemStatus.setItemId(itemId + getItemId(attributes));
        this.objectStack.push(itemStatus);
    }

    private void popItemStatus() {
        ItemStatus itemStatus = null;
        try {
            if (!this.objectStack.isEmpty()) {
                itemStatus = this.objectStack.pop();
                if (itemStatus.getItemName() != null && itemStatus.getStatus() != null) {
                    alItemsSet.add(itemStatus);
                }
            }
        } catch (EmptyStackException e) {
        }
    }

    private void peekItemStatus(String itemName, String itemType, String status) {
        if (!this.objectStack.isEmpty()) {
            ItemStatus itemStatus = this.objectStack.peek();
            itemStatus.setItemName(itemName);
            itemStatus.setItemType(itemType);
            itemStatus.setStatus(status);
        }
    }

    private boolean hasStatus(char ch[], int start, int length) {
        return new String(ch, start, length).trim().length() > 0;
    }

    private String currentElement() {
        return this.elementStack.peek();
    }

    private String getItemId(Attributes attribute) {
        itemId = "";
        if (attribute != null && attribute.getLength() > 0) {
            if (!attribute.getValue("item").equals("null") && attribute.getValue("item") != null) {
                itemId = "_" + attribute.getValue("item");
            }
        }
        return itemId;
    }

    private void getItemStatus(Attributes attribute) {
        if (attribute != null && attribute.getLength() > 0) {
            if (!attribute.getValue("status").equals("null") && attribute.getValue("status") != null) {
                setItemStatus(attribute.getValue("status"));
            }
        }
    }

    private void getItemConnected(Attributes attribute) {
        if (attribute != null && attribute.getLength() > 0) {
            if (!attribute.getValue("connected").equals("null") && attribute.getValue("connected") != null) {
                setItemStatus(attribute.getValue("connected"));
            }
        }
    }

    public String getItemStatus() {
        return itemStatus;
    }

    public void setItemStatus(String itemStatus) {
        this.itemStatus = itemStatus;
    }
}

