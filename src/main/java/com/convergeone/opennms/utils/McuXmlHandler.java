package com.convergeone.opennms.utils;

import org.xml.sax.Attributes;
import org.xml.sax.SAXException;
import org.xml.sax.helpers.DefaultHandler;

import java.util.*;

public class McuXmlHandler extends DefaultHandler {
    private List<ItemStatus> alItems = null;
    private Set<ItemStatus> alItemsSet = null;
    boolean isMember = false;
    private Stack<String> elementStack = null;
    private Stack<ItemStatus> objectStack = null;
    String queryType = "";
    Random rand = new Random();

    McuXmlHandler(String type) {
        super();
        this.queryType = type;
    }

    public List<ItemStatus> getAlItems() {
        Collections.sort(alItems);
        return alItems;
    }

    public void startDocument() throws SAXException {
        alItemsSet = new HashSet<>();
        elementStack = new Stack<>();
        objectStack = new Stack<>();
    }

    public void endDocument() throws SAXException {
        alItems = new ArrayList<>(alItemsSet);
        elementStack = null;
        objectStack = null;
        alItemsSet = null;
    }


    @Override
    public void startElement(String uri, String localName, String qName, Attributes attributes) throws SAXException {
        this.elementStack.push(qName);
        if (qName.equalsIgnoreCase("member")) {
            isMember = true;
        }
    }

    public void characters(char ch[], int start, int length) throws SAXException {
        if (isMember) {
            setMemberName(ch, start, length);
        }
    }

    @Override
    public void endElement(String uri, String localName, String qName) throws SAXException {
        if (qName.equalsIgnoreCase("member")) {
            popItemStatus();
            isMember = false;
        }
    }

    private void setMemberName(char ch[], int start, int length) throws SAXException {
        FieldsKeyMap fieldsKeyMap = new FieldsKeyMap(getQueryType());
        Map queryMap = fieldsKeyMap.getMap();
        int randomNum = rand.nextInt((10 - 0) + 1) + 0;
        if (hasStatus(ch, start, length)) {
            ItemStatus itemStatus = new ItemStatus();
            if (isMember && "name".equalsIgnoreCase(currentElement())) {
                String itemName = new String(ch, start, length).trim();
                if (queryMap.containsKey(itemName)) {
                    FieldData fieldData = (FieldData) queryMap.get(itemName);
                    itemStatus.setItemId(fieldData.getItemId());
                    itemStatus.setItemName(fieldData.getItemName());
                    itemStatus.setItemType(fieldData.getItemType());
                    this.objectStack.push(itemStatus);
                }
            } else if (isMember && "string".equalsIgnoreCase(currentElement())) {
                setStatusValue(ch, start, length);
            } else if (isMember && "int".equalsIgnoreCase(currentElement())) {
                setStatusValue(ch, start, length);
            }
        }
    }

    private void setStatusValue(char ch[], int start, int length) throws SAXException {
        if (!this.objectStack.isEmpty()) {
            ItemStatus itemStatus = this.objectStack.peek();
            itemStatus.setStatus(new String(ch, start, length).trim());
            if ("".equals(itemStatus.getStatus()) || "null".equals(itemStatus)) {
                itemStatus.setStatus("BLANK");
            }
        }
    }

    private void popItemStatus() {
        ItemStatus itemStatus = null;
        try {
            if (!this.objectStack.isEmpty()) {
                itemStatus = this.objectStack.pop();
                if (itemStatus.getItemName() != null && itemStatus.getStatus() != null) {
                    alItemsSet.add(itemStatus);
                }
            }
        } catch (EmptyStackException e) {
        }
    }

    private boolean hasStatus(char ch[], int start, int length) {
        return new String(ch, start, length).trim().length() > 0;
    }

    private String currentElement() {
        return this.elementStack.peek();
    }

    public String getQueryType() {
        return queryType;
    }

}

